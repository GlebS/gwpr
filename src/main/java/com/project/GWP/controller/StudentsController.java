package com.project.GWP.controller;

import com.project.GWP.domain.Students;

import com.project.GWP.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/test", produces = "application/json; charset=UTF-8")
public class StudentsController {

    @Autowired
    private StudentsRepository studentsRepository;

    @GetMapping
    public ModelAndView displayStudents( Map<String, Object> model) {
        Iterable<Students> studentslist = studentsRepository.findAll();
        model.put("studentlist", studentslist);
        return new ModelAndView("index", model);
    }


    @PostMapping(value = "/save")
    public ModelAndView addStudents(@RequestParam String studentSurname, @RequestParam String studentName, @RequestParam String studentPatronymic, @RequestParam int studentMark, Map<String, Object> model) {
        Students students = new Students(studentSurname, studentName, studentPatronymic, studentMark);
        studentsRepository.save(students);  // сохранение в БД

        List<Students> studentlist = studentsRepository.findAll();
        Map<String, Object> params = new HashMap<>();
        params.put("students", studentlist); // вывод на экран

        return new ModelAndView("index", params);
    }


    private Students generateArticle(String surname, String name, String patronymic, int mark) {
        Students students = new Students();

        students.setSurname(surname);
        students.setName(name);
        students.setPatronymic(patronymic);
        students.setMark(mark);

        return students;
    }

}






//            Message message = new Message(studentSurname, studentName, studentPatronymic, studentMark);
//
//            messageRepository.save(message);

//        List<Students> students = IntStream.range(0, 1)
//                .mapToObj(i -> generateArticle("Ivanov", "Ivan", "Ivanovich", 1, 10 ))
//                .collect(Collectors.toList());
//
//        model.put("students", students);