package com.project.GWP.repository;

import com.project.GWP.domain.Students;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentsRepository  extends JpaRepository<Students, Long>  {

}


