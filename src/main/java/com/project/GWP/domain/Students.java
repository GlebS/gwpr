package com.project.GWP.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Data

public class Students {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)

    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private int mark;


    public Students(){
    }


    public Students(String studentSurname, String studentName, String studentPatronymic, int studentMark){

        this.surname = studentSurname;
        this.name = studentName;
        this.patronymic = studentPatronymic;
        this.mark = studentMark;
    }


}
